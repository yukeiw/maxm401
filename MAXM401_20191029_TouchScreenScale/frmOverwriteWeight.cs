﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAXM401_20191029_TouchScreenScale
{
    public partial class frmOverwriteWeight : Form
    {
        private int response;

        public frmOverwriteWeight()
        {
            InitializeComponent();
        }

        private void cmdReWeigh_Click(object sender, EventArgs e)
        {
            response = RESPONSE_CLEAR;
            this.Close();
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            response = RESPONSE_ADD;
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            response = RESPONSE_CANCEL;
            this.Close();

        }

        public const int RESPONSE_CANCEL = 0;
        public const int RESPONSE_CLEAR = 1;
        public const int RESPONSE_ADD = 2;
        
        public int getResponse() { return response; }


    }
}
