﻿namespace MAXM401_20191029_TouchScreenScale
{
    partial class frmIDScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessageLabel = new System.Windows.Forms.Label();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.cmdOK = new System.Windows.Forms.Button();
            this.pnlKeypad = new System.Windows.Forms.FlowLayoutPanel();
            this.cmd1 = new System.Windows.Forms.Button();
            this.cmd2 = new System.Windows.Forms.Button();
            this.cmd3 = new System.Windows.Forms.Button();
            this.cmd4 = new System.Windows.Forms.Button();
            this.cmd5 = new System.Windows.Forms.Button();
            this.cmd6 = new System.Windows.Forms.Button();
            this.cmd7 = new System.Windows.Forms.Button();
            this.cmd8 = new System.Windows.Forms.Button();
            this.cmd9 = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmd0 = new System.Windows.Forms.Button();
            this.cmdClear = new System.Windows.Forms.Button();
            this.lblNumber = new System.Windows.Forms.Label();
            this.pnlButtons.SuspendLayout();
            this.pnlKeypad.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMessageLabel
            // 
            this.lblMessageLabel.BackColor = System.Drawing.Color.Transparent;
            this.lblMessageLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageLabel.ForeColor = System.Drawing.Color.Black;
            this.lblMessageLabel.Location = new System.Drawing.Point(0, 0);
            this.lblMessageLabel.Name = "lblMessageLabel";
            this.lblMessageLabel.Size = new System.Drawing.Size(657, 78);
            this.lblMessageLabel.TabIndex = 0;
            this.lblMessageLabel.Text = "請輸入件數";
            this.lblMessageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.cmdOK);
            this.pnlButtons.Controls.Add(this.pnlKeypad);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 153);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(657, 432);
            this.pnlButtons.TabIndex = 3;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdOK.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.Location = new System.Drawing.Point(499, 0);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(0);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(158, 432);
            this.cmdOK.TabIndex = 9;
            this.cmdOK.Text = "確認";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // pnlKeypad
            // 
            this.pnlKeypad.Controls.Add(this.cmd1);
            this.pnlKeypad.Controls.Add(this.cmd2);
            this.pnlKeypad.Controls.Add(this.cmd3);
            this.pnlKeypad.Controls.Add(this.cmd4);
            this.pnlKeypad.Controls.Add(this.cmd5);
            this.pnlKeypad.Controls.Add(this.cmd6);
            this.pnlKeypad.Controls.Add(this.cmd7);
            this.pnlKeypad.Controls.Add(this.cmd8);
            this.pnlKeypad.Controls.Add(this.cmd9);
            this.pnlKeypad.Controls.Add(this.cmdCancel);
            this.pnlKeypad.Controls.Add(this.cmd0);
            this.pnlKeypad.Controls.Add(this.cmdClear);
            this.pnlKeypad.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlKeypad.Location = new System.Drawing.Point(0, 0);
            this.pnlKeypad.Name = "pnlKeypad";
            this.pnlKeypad.Size = new System.Drawing.Size(499, 432);
            this.pnlKeypad.TabIndex = 2;
            // 
            // cmd1
            // 
            this.cmd1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd1.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd1.Location = new System.Drawing.Point(0, 0);
            this.cmd1.Margin = new System.Windows.Forms.Padding(0);
            this.cmd1.Name = "cmd1";
            this.cmd1.Size = new System.Drawing.Size(166, 108);
            this.cmd1.TabIndex = 0;
            this.cmd1.Text = "1";
            this.cmd1.UseVisualStyleBackColor = true;
            this.cmd1.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd2
            // 
            this.cmd2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd2.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd2.Location = new System.Drawing.Point(166, 0);
            this.cmd2.Margin = new System.Windows.Forms.Padding(0);
            this.cmd2.Name = "cmd2";
            this.cmd2.Size = new System.Drawing.Size(166, 108);
            this.cmd2.TabIndex = 1;
            this.cmd2.Text = "2";
            this.cmd2.UseVisualStyleBackColor = true;
            this.cmd2.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd3
            // 
            this.cmd3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd3.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd3.Location = new System.Drawing.Point(332, 0);
            this.cmd3.Margin = new System.Windows.Forms.Padding(0);
            this.cmd3.Name = "cmd3";
            this.cmd3.Size = new System.Drawing.Size(166, 108);
            this.cmd3.TabIndex = 2;
            this.cmd3.Text = "3";
            this.cmd3.UseVisualStyleBackColor = true;
            this.cmd3.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd4
            // 
            this.cmd4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd4.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd4.Location = new System.Drawing.Point(0, 108);
            this.cmd4.Margin = new System.Windows.Forms.Padding(0);
            this.cmd4.Name = "cmd4";
            this.cmd4.Size = new System.Drawing.Size(166, 108);
            this.cmd4.TabIndex = 3;
            this.cmd4.Text = "4";
            this.cmd4.UseVisualStyleBackColor = true;
            this.cmd4.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd5
            // 
            this.cmd5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd5.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd5.Location = new System.Drawing.Point(166, 108);
            this.cmd5.Margin = new System.Windows.Forms.Padding(0);
            this.cmd5.Name = "cmd5";
            this.cmd5.Size = new System.Drawing.Size(166, 108);
            this.cmd5.TabIndex = 4;
            this.cmd5.Text = "5";
            this.cmd5.UseVisualStyleBackColor = true;
            this.cmd5.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd6
            // 
            this.cmd6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd6.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd6.Location = new System.Drawing.Point(332, 108);
            this.cmd6.Margin = new System.Windows.Forms.Padding(0);
            this.cmd6.Name = "cmd6";
            this.cmd6.Size = new System.Drawing.Size(166, 108);
            this.cmd6.TabIndex = 5;
            this.cmd6.Text = "6";
            this.cmd6.UseVisualStyleBackColor = true;
            this.cmd6.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd7
            // 
            this.cmd7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd7.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd7.Location = new System.Drawing.Point(0, 216);
            this.cmd7.Margin = new System.Windows.Forms.Padding(0);
            this.cmd7.Name = "cmd7";
            this.cmd7.Size = new System.Drawing.Size(166, 108);
            this.cmd7.TabIndex = 6;
            this.cmd7.Text = "7";
            this.cmd7.UseVisualStyleBackColor = true;
            this.cmd7.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd8
            // 
            this.cmd8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd8.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd8.Location = new System.Drawing.Point(166, 216);
            this.cmd8.Margin = new System.Windows.Forms.Padding(0);
            this.cmd8.Name = "cmd8";
            this.cmd8.Size = new System.Drawing.Size(166, 108);
            this.cmd8.TabIndex = 7;
            this.cmd8.Text = "8";
            this.cmd8.UseVisualStyleBackColor = true;
            this.cmd8.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmd9
            // 
            this.cmd9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd9.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd9.Location = new System.Drawing.Point(332, 216);
            this.cmd9.Margin = new System.Windows.Forms.Padding(0);
            this.cmd9.Name = "cmd9";
            this.cmd9.Size = new System.Drawing.Size(166, 108);
            this.cmd9.TabIndex = 8;
            this.cmd9.Text = "9";
            this.cmd9.UseVisualStyleBackColor = true;
            this.cmd9.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.BackColor = System.Drawing.Color.Red;
            this.cmdCancel.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.ForeColor = System.Drawing.Color.White;
            this.cmdCancel.Location = new System.Drawing.Point(0, 324);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(0);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(166, 108);
            this.cmdCancel.TabIndex = 9;
            this.cmdCancel.Text = "退出";
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmd0
            // 
            this.cmd0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd0.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd0.Location = new System.Drawing.Point(166, 324);
            this.cmd0.Margin = new System.Windows.Forms.Padding(0);
            this.cmd0.Name = "cmd0";
            this.cmd0.Size = new System.Drawing.Size(166, 108);
            this.cmd0.TabIndex = 10;
            this.cmd0.Text = "0";
            this.cmd0.UseVisualStyleBackColor = true;
            this.cmd0.Click += new System.EventHandler(this.cmdKeyPress_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.BackColor = System.Drawing.Color.Yellow;
            this.cmdClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClear.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClear.Location = new System.Drawing.Point(332, 324);
            this.cmdClear.Margin = new System.Windows.Forms.Padding(0);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(166, 108);
            this.cmdClear.TabIndex = 11;
            this.cmdClear.Text = "清除";
            this.cmdClear.UseVisualStyleBackColor = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // lblNumber
            // 
            this.lblNumber.BackColor = System.Drawing.Color.Black;
            this.lblNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumber.Font = new System.Drawing.Font("Yu Gothic UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.ForeColor = System.Drawing.Color.Lime;
            this.lblNumber.Location = new System.Drawing.Point(0, 78);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(657, 75);
            this.lblNumber.TabIndex = 4;
            this.lblNumber.Text = "0";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmIDScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(657, 585);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.lblMessageLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmIDScan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmIDScan";
            this.pnlButtons.ResumeLayout(false);
            this.pnlKeypad.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessageLabel;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.FlowLayoutPanel pnlKeypad;
        private System.Windows.Forms.Button cmd1;
        private System.Windows.Forms.Button cmd2;
        private System.Windows.Forms.Button cmd3;
        private System.Windows.Forms.Button cmd4;
        private System.Windows.Forms.Button cmd5;
        private System.Windows.Forms.Button cmd6;
        private System.Windows.Forms.Button cmd7;
        private System.Windows.Forms.Button cmd8;
        private System.Windows.Forms.Button cmd9;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmd0;
        private System.Windows.Forms.Button cmdClear;
        private System.Windows.Forms.Label lblNumber;
    }
}