﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

namespace MAXM401_20191029_TouchScreenScale
{
    public class MAXMSQL
    {
        public static PostObject getPostObject()
        {
            return new PostObject();
        }

        public static void savePostObject (String ConnectionString, PostObject postObject)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                IF EXISTS (SELECT 1 FROM postobject where DocEntry = {0})

                UPDATE postobject set CTN={1}, picktype={2}, [user]='{3}' WHERE docEntry = {0}
                
                ELSE

                INSERT INTO postobject (DocEntry, CTN, picktype, [user]) VALUES ({0}, {1}, {2}, '{3}');

            ", postObject.DocEntry, postObject.CTN, postObject.PickType, postObject.User);

            Debug.WriteLine(commandStr);
            

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
            
        }

        public static void insertIgnorePostObject(String ConnectionString, PostObject postObject)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                IF NOT EXISTS (SELECT 1 FROM postobject where DocEntry = {0})
                    INSERT INTO postobject (DocEntry, CTN, picktype, [user]) VALUES ({0}, {1}, {2}, '{3}');",

                   postObject.DocEntry, postObject.CTN, postObject.PickType, postObject.User);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void SaveLine (String ConnectionString, Line line, int DocNum, String mProcess)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                IF EXISTS (SELECT 1 FROM line where LineNum = {0} AND PostObjectDocEntry = {2})

                UPDATE line set Weight='{1}', Process={3} WHERE LineNum = {0} AND PostObjectDocEntry = {2} 

                ELSE

                INSERT INTO line (LineNum, Weight, PostObjectDocEntry, Process) VALUES ({0}, '{1}', {2}, {3});

            ", line.LineNum, line.Weigh, DocNum, mProcess);

            Debug.WriteLine(commandStr);


            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();

        }

        public static void insertIgnoreCatchWeight(String ConnectionString, Line line, Catchweight catchweight, int DocNum)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                IF NOT EXISTS (SELECT 1 FROM catchweight WHERE Line = {0} AND DocEntry = {1})
                    INSERT INTO catchweight (Line, DocEntry) VALUES ({0},{1})"
                
                    , line.LineNum, DocNum);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void insertIgnoreLine(String ConnectionString, Line line, int DocNum, String mProcess)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                IF NOT EXISTS (SELECT 1 FROM line WHERE LineNum = {0} AND PostObjectDocEntry = {1} AND Process = {2})
                    INSERT INTO line (LineNum, PostObjectDocEntry, Process) VALUES ({0},{1},{2})"

                    , line.LineNum, DocNum, mProcess);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void updateCatchWeight(String ConnectionString, Line line, Catchweight catchweight, int DocNum)
        { 
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                UPDATE catchweight SET 
                    QTY1={2},
                    QTY2={3},
                    UOM1='{4}',
                    UOM2='{5}'
                WHERE Line = {0} AND DocEntry = {1};",
                line.LineNum, DocNum, catchweight.QTY1, catchweight.QTY2, catchweight.UOM1, catchweight.UOM2);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void SetOKPressed(String ConnectionString, Line line, int DocNum, int okPressed)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                UPDATE catchweight SET 
                    okPressed = {2}
                WHERE Line = {0} AND DocEntry = {1};",
                line.LineNum, DocNum, okPressed);

            File.WriteAllText("SQL.txt", commandStr);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }
    
        public static string getConnectionString(String IP, int port, String Database, String ID, String Password)
        {
            
            return String.Format("Server = {0};  Database = {2}; User Id = {3}; Password = {4};", IP, port, Database, ID, Password);
        }

        public static PostObject getPostObject(String ConnectionString, int DocEntry, ref Boolean[] okButtonPressed, String mProcess)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                SELECT * FROM postObject WHERE DocEntry = {0}",
                DocEntry);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();
            
            if (!reader.HasRows)
            {
                return new PostObject();
            }

            reader.Read();

            PostObject returnPostObject = new PostObject();
            returnPostObject.CreateDateTime = reader["CreateDateTime"].ToString();
            returnPostObject.DocEntry = (Int32)reader["DocEntry"];
            returnPostObject.PickType = (Int32)reader["PickType"];
            returnPostObject.CTN = (Int32)reader["CTN"];
            returnPostObject.User = reader["User"].ToString();


            returnPostObject.Lines = getLines(ConnectionString, DocEntry, ref okButtonPressed, mProcess);

            conn.Close();

            return returnPostObject;
        }

        internal static void removeCatchWeight(String ConnectionString, int lineNum, int docNum)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                DELETE FROM catchweight WHERE Line = {0} AND DocEntry = {1}
                ;",
                lineNum, docNum);

            File.WriteAllText("SQL.txt", commandStr);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        internal static void removeLine(String ConnectionString, int lineNum, int docNum, string mProcess)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                DELETE FROM line WHERE LineNum = {0} AND PostObjectDocEntry = {1} AND Process = {2}
                ;",
                lineNum, docNum, mProcess);

            File.WriteAllText("SQL.txt", commandStr);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static Line[] getLines(String ConnectionString, int DocEntry, ref Boolean[] okButtonPressed, String mProcess)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                SELECT * FROM line AS a
                LEFT JOIN
                catchweight AS b
                ON
                b.DocEntry = a.PostObjectDocEntry AND b.Line = a.LineNum
                WHERE (PostObjectDocEntry = {0} AND Process = {1});",
                DocEntry, mProcess);

            Debug.WriteLine(commandStr);

            
            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            List<Line> lineList = new List<Line>();
            List<Boolean> okButtonPressedList = new List<Boolean>();

            while (reader.Read())
            {
                
                Line line = new Line();
                line.LineNum = (Int32)reader["LineNum"];
                line.Weigh = reader["Weight"].ToString();
                line.CatchWeights = new Catchweight[1];
                line.CatchWeights[0] = new Catchweight();
                line.CatchWeights[0].QTY1 = float.Parse(reader["QTY1"].ToString());
                line.CatchWeights[0].QTY2 = float.Parse(reader["QTY2"].ToString());
                line.CatchWeights[0].UOM1 = reader["UOM1"].ToString();
                line.CatchWeights[0].UOM2 = reader["UOM2"].ToString();

                lineList.Add(line);
                okButtonPressedList.Add(ConvertIntToBool((Int32)reader["okPressed"]));

            }

            Debug.WriteLine("Line Count:" + lineList.Count);

            okButtonPressed = okButtonPressedList.ToArray();

            conn.Close();

            return lineList.ToArray();

        }

        private static bool ConvertIntToBool(int v)
        {
            return (v==1);
        }
    }
}
