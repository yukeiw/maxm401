﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace MAXM401_20191029_TouchScreenScale
{
    public partial class frmMessage : Form
    {
        Boolean dismissable = true;
        int secondsToClose;

        public frmMessage(String Text) : this(Text, true, 0)
        {

        }

        public frmMessage(String Text, Boolean dismissable) : this(Text, dismissable, 0)
        {

        }

        public frmMessage(String Text, Boolean dismissable, int secondsToClose)
        {

            InitializeComponent();

            lblMessage.Text = Text;
            this.dismissable = dismissable;
            this.secondsToClose = secondsToClose;

            if (secondsToClose != 0)
            {
                Task.Run(() => {
                    Thread.Sleep(secondsToClose * 1000);
                    this.Close();
                });
            }
        }

        private void lblMessage_Click(object sender, EventArgs e)
        {
            if (dismissable)
            {
                this.Close();
            }
        }
    }
}
