﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using ACUR401_20190227_HPAPI;
using System.Diagnostics;
using MNLABEL;
using System.Xml.Serialization;
using System.Deployment.Application;

namespace MAXM401_20191029_TouchScreenScale
{
    public partial class frmMain : Form
    {

        public static string LOG_FILENAME = "log.txt";

        /*
        string getAddr = "http://10.33.1.70:3201/scale/getSOMAInfo";
        string postAddr = "http://10.33.1.70:3201/scale/SOMALotNum";

        string printerName = "TSC MH340";
        */

        string getAddr = "";
        string postAddr = "";
        string printerName = "";
        string postReleaseAddr = "";
        public HPAPI scale;
        public ItemList itemList;
        public Data currentData;
        List<ItemRow> itemRows = new List<ItemRow>();
        int currentPage = 0;
        int maxPage = 0;
        static int RowsPerPage = 4;
        public PostObject postObject;
        public Boolean[] okButtonPressed;
        String mProcess = "";

        String deviceID;

        String DB_IP = "";
        int DB_port;
        String DB_login = "";
        String DB_password = "";
        public String connectionString = "";

        public int DocEntry;
        public int DocNum;

        string BarcodeInput;


        public frmMain()
        {
            /*
            InitialisationXML init = new InitialisationXML();
            XmlSerializer xmlSerializer = new XmlSerializer(init.GetType());
            StreamReader reader = new StreamReader("init.xml");
            init = (InitialisationXML)xmlSerializer.Deserialize(reader);
            */

            getAddr = Properties.Settings.Default.getHTTP;
            postAddr = Properties.Settings.Default.postHTTP;
            postReleaseAddr = Properties.Settings.Default.postReleaseHTTP;
            printerName = Properties.Settings.Default.printerName;
            deviceID = Properties.Settings.Default.deviceID;

            DB_IP = Properties.Settings.Default.DB_IP;
            DB_port = Properties.Settings.Default.DB_port;
            DB_login = Properties.Settings.Default.DB_login;
            DB_password = Properties.Settings.Default.DB_password;
            connectionString = MAXMSQL.getConnectionString(DB_IP, DB_port, "MAXM401", DB_login, DB_password);

            InitializeComponent();
            scale = new HPAPI("registry", "MAXM401");
            // scale = new HPAPI("test");
            scale.start();

            tmrWeight.Enabled = true;


            try
            {
                lblVersion.Text = string.Format("{0}",
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4));

            } catch (Exception ex) { }
            


        }

        private void processInput(String ScannedInput)
        {

            if (ScannedInput.IndexOf(",") == -1)
            {
                BarcodeInput = "";
                lblInvoiceNo.Text = "";
                return;
            }

            ScannedInput = ScannedInput.Replace("*", "");

            String[] data = ScannedInput.Split(',');
            String docNumStr = data[0];
            mProcess = data[1];

            Boolean parseDocNum = Int32.TryParse(data[0], out DocNum);

            if (!parseDocNum)
            {
                BarcodeInput = "";
                lblInvoiceNo.Text = "";
                return;
            }

            if ((mProcess != "1") && (mProcess != "2"))
            {
                BarcodeInput = "";
                lblInvoiceNo.Text = "";
                return;
            }

            data[0] = data[0].Replace("*", "");

            if (mProcess == "1")
            {
                lblInvoiceNo.Text = docNumStr + "[散貨]";
            }
            else
            {
                lblInvoiceNo.Text = docNumStr + "[加工]";
            }


            Order order = new Order();
            order.DocNum = docNumStr;
            order.Process = mProcess;

            scale.pause();

            while (scale.isOpen())
            {

            }

            // Below code is for production
            Task<String> t = Task.Run(() => getHTTP(order, getAddr));


            if (t.Result.Equals(""))
            {

                frmMessage form = new frmMessage("單號錯誤. Err(3).", true);
                form.ShowDialog();

                BarcodeInput = "";
                lblInvoiceNo.Text = "";
                scale.unpause();

                return;
            }




            // Below code is for testing
            /*
            String test = File.ReadAllText("defaultReturnJSONString.txt", Encoding.UTF8);
            itemList = JsonConvert.DeserializeObject<ItemList>(test);
            */



            // commented out for testing

            try
            {
                File.WriteAllText(LOG_FILENAME, t.Result);
                itemList = JsonConvert.DeserializeObject<ItemList>(t.Result);
            }
            catch (Exception ex)
            {

                frmMessage form = new frmMessage("單號錯誤. Err(2).", true);
                form.ShowDialog();

                BarcodeInput = "";
                lblInvoiceNo.Text = "";
                scale.unpause();

                return;
            }




            if (itemList.total == 0)
            {
                BarcodeInput = "";
                lblInvoiceNo.Text = "";

                frmMessage form = new frmMessage("此單號不存在. Err(1).", true);
                form.ShowDialog();
                scale.unpause();


                return;
            }

            foreach (Data fdata in itemList.data)
            {

                if (fdata.Processing.Equals("2"))
                {
                    fdata.Weigh = "Y";
                }
            }

            scale.unpause();

            enableAllButtons(true);

            LoadItems();

        }

        private void LoadItems()
        {
            DocEntry = itemList.data[0].DocEntry;

            lblCustomerName.Text = itemList.data[0].CardName;
            lblDate.Text = itemList.data[0].DocDate;

            currentPage = 0;

            maxPage = (itemList.data.Length - 1) / RowsPerPage + 1;

            if (maxPage < 1)
            {
                maxPage = 1;
            }


            // inserting new line catch weight, and updating postobject
            postObject = new PostObject();
            postObject.Lines = new Line[itemList.data.Length];
            okButtonPressed = new Boolean[itemList.data.Length];
            postObject.DocEntry = DocEntry;
            postObject.PickType = Int32.Parse(mProcess);
            postObject.Device = deviceID;
            postObject.CreateDateTime = "";
            postObject.User = "";

            MAXMSQL.insertIgnorePostObject(connectionString, postObject);

            for (int i = 0; i < postObject.Lines.Length; i++)
            {
                postObject.Lines[i] = new Line();
                postObject.Lines[i].Weigh = itemList.data[i].Weigh;
                postObject.Lines[i].CatchWeights = new Catchweight[1];
                postObject.Lines[i].CatchWeights[0] = new Catchweight();
                postObject.Lines[i].CatchWeights[0].UOM1 = "";
                postObject.Lines[i].CatchWeights[0].UOM2 = "";
                postObject.Lines[i].LineNum = itemList.data[i].LineNum;
                
                MAXMSQL.SaveLine(connectionString, postObject.Lines[i], DocEntry, mProcess);
                MAXMSQL.insertIgnoreCatchWeight(connectionString, postObject.Lines[i], postObject.Lines[i].CatchWeights[0], DocEntry);
            }


            // check for line items that no longer exist, if so, remove from SQL
            postObject = MAXMSQL.getPostObject(connectionString, DocEntry, ref okButtonPressed, mProcess);
            postObject.PickType = Int32.Parse(mProcess);

            for (int i = 0; i < postObject.Lines.Length; i++)
            {
                Boolean objectFound = false;

                foreach (Data fdata in itemList.data)
                {
                    if (postObject.Lines[i].LineNum == fdata.LineNum)
                    {
                        objectFound = true;
                        break;
                    }
                }

                if (!objectFound)
                {
                    MAXMSQL.removeLine(connectionString, postObject.Lines[i].LineNum, DocEntry, mProcess);
                    MAXMSQL.removeCatchWeight(connectionString, postObject.Lines[i].LineNum, DocEntry);
                }
            }



            /*
            // this is for if the postObject already created, but no lines.
            // this would happen if the same doc Number is used already but now its used for a different process
            if ((postObject.CreateDateTime != null) && (postObject.Lines.Length == 0))
            {
                postObject.Lines = new Line[itemList.data.Length];
                okButtonPressed = new Boolean[itemList.data.Length];

                for (int i = 0; i < itemList.data.Length; i++)
                {
                    postObject.Lines[i] = new Line();
                    postObject.Lines[i].Weigh = itemList.data[i].Weigh;
                    postObject.Lines[i].CatchWeights = new Catchweight[1];
                    postObject.Lines[i].CatchWeights[0] = new Catchweight();
                    postObject.Lines[i].CatchWeights[0].UOM1 = "";
                    postObject.Lines[i].CatchWeights[0].UOM2 = "";
                    postObject.Lines[i].LineNum = itemList.data[i].LineNum;

                    MAXMSQL.SaveLine(connectionString, postObject.Lines[i], DocEntry, mProcess);
                    MAXMSQL.insertIgnoreCatchWeight(connectionString, postObject.Lines[i], postObject.Lines[i].CatchWeights[0], DocEntry);
                }
            }
            */



            // this is for testing
            // postObject = new PostObject();

            showItems();

            refreshPageInfo();

            checkIfCompleted();
        }

        private void showItems()
        {

            tblLayout.Controls.Clear();

            if ((maxPage - 1) == currentPage)
            {
                int iteration = itemList.data.Length % RowsPerPage;
                if (iteration == 0)
                {
                    iteration = RowsPerPage;
                }



                for (int i = 0; i < iteration; i++)
                {
                    int index = currentPage * RowsPerPage + i;

                    ItemRow itemRow = new ItemRow(this, itemList.data[index]);
                    itemRows.Add(itemRow);

                    foreach (Line line in postObject.Lines)
                    {

                        if (line.LineNum == itemList.data[index].LineNum)
                        {

                            if (line.CatchWeights[0].QTY1 != 0)
                            {
                                itemRow.WeighButton.setWeighingButtonText(line.CatchWeights[0].QTY1 + " kg\r\n" + Math.Round(line.CatchWeights[0].QTY1 * 2.205, 2) + " lb");
                            }

                            if (line.CatchWeights[0].QTY2 != 0)
                            {
                                itemRow.ProcessButton.setWeighingButtonText(line.CatchWeights[0].QTY2 + " kg\r\n" + Math.Round(line.CatchWeights[0].QTY2 * 2.205, 2) + " lb");
                            }

                            if (okButtonPressed[index])
                            {
                                itemRow.confirmButton.BackColor = Color.Lime;
                            }

                            break;
                        }


                    }




                    if (itemList.data[index].Status == ("C"))
                    {
                        itemRow.WeighButton.Enabled = false;
                        itemRow.ProcessButton.Enabled = false;
                        itemRow.confirmButton.Enabled = false;

                        itemRow.WeighButton.BackColor = Color.LightBlue;
                        itemRow.ProcessButton.BackColor = Color.LightBlue;
                        itemRow.confirmButton.BackColor = Color.LightBlue;

                    }


                    tblLayout.Controls.Add(itemRow.ItemDescription);
                    tblLayout.Controls.Add(itemRow.ItemOrderQty);
                    tblLayout.Controls.Add(itemRow.WeighButton);
                    tblLayout.Controls.Add(itemRow.ProcessButton);
                    tblLayout.Controls.Add(itemRow.confirmButton);
                    tblLayout.Controls.Add(itemRow.dummyControl);

                }
            }
            else
            {
                for (int i = 0; i < RowsPerPage; i++)
                {
                    int index = currentPage * RowsPerPage + i;

                    ItemRow itemRow = new ItemRow(this, itemList.data[index]);
                    itemRows.Add(itemRow);

                    foreach (Line line in postObject.Lines)
                    {

                        if (itemList.data[index].LineNum == line.LineNum)
                        {
                            if (line.CatchWeights[0].QTY1 != 0)
                            {
                                itemRow.WeighButton.setWeighingButtonText(line.CatchWeights[0].QTY1 + " kg\r\n" + Math.Round(line.CatchWeights[0].QTY1 * 2.205, 2) + " lb");
                            }

                            if (line.CatchWeights[0].QTY2 != 0)
                            {
                                itemRow.ProcessButton.setWeighingButtonText(line.CatchWeights[0].QTY2 + " kg\r\n" + Math.Round(line.CatchWeights[0].QTY2 * 2.205, 2) + " lb");
                            }

                            if (okButtonPressed[index])
                            {
                                itemRow.confirmButton.BackColor = Color.Lime;
                            }

                            break;
                        }
                    }

                    if (itemList.data[index].Status == ("C"))
                    {
                        itemRow.WeighButton.Enabled = false;
                        itemRow.ProcessButton.Enabled = false;
                        itemRow.confirmButton.Enabled = false;

                        itemRow.WeighButton.BackColor = Color.LightBlue;
                        itemRow.ProcessButton.BackColor = Color.LightBlue;
                        itemRow.confirmButton.BackColor = Color.LightBlue;

                    }


                    tblLayout.Controls.Add(itemRow.ItemDescription);
                    tblLayout.Controls.Add(itemRow.ItemOrderQty);
                    tblLayout.Controls.Add(itemRow.WeighButton);
                    tblLayout.Controls.Add(itemRow.ProcessButton);
                    tblLayout.Controls.Add(itemRow.confirmButton);
                    tblLayout.Controls.Add(itemRow.dummyControl);

                }
            }


        }

        private void refreshPageInfo()
        {
            lblPageNo.Text = String.Format("第 {0} / {1} 頁", currentPage + 1, maxPage);
        }

        public void clearSelected()
        {
            foreach (ItemRow itemRow in itemRows)
            {
                itemRow.ItemDescription.BackColor = Color.Transparent;
                itemRow.ItemOrderQty.BackColor = Color.Transparent;
                itemRow.WeighButton.BackColor = Color.Transparent;
                itemRow.ProcessButton.BackColor = Color.Transparent;
                itemRow.confirmButton.BackColor = Color.Transparent;
            }
        }

        private void enableAllButtons(Boolean isEnable)
        {
            cmdComplete.Enabled = isEnable;
            cmdDown.Enabled = isEnable;
            cmdPrint.Enabled = isEnable;
            cmdUp.Enabled = isEnable;
            cmdReleaseInvoice.Enabled = isEnable;
            cmdCancel.Enabled = isEnable;

            if (!isEnable)
            {
                cmdComplete.BackColor = SystemColors.Control;
                lblPageNo.Text = "";
            }


        }

        private static async Task<String> postHTTP(string jsonString, string addr)
        {
            File.WriteAllText("Debug_postHTTP_jsonString.txt", jsonString);

            HttpClient client = new HttpClient();

            StringContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(addr, content);

            string responseString = await response.Content.ReadAsStringAsync();

            File.WriteAllText("Debug_postHTTP_responseString.txt", responseString);

            return responseString;
        }

        private static async Task<String> getHTTP(Order order, string addr)
        {
            String Options = String.Format("?DocNum={0}&Process={1}", order.DocNum, order.Process);
            //String Options = String.Format("?DocNum={0}", order.DocNum, order.Process);
            String url = addr + Options;


            WebRequest request = WebRequest.Create(url);

            File.WriteAllText("getHTTPtxt", url);

            WebResponse response = request.GetResponse();

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            String XmlString = reader.ReadToEnd();

            if (XmlString.Equals(""))
            {
                return "";
            }

            reader.Close();
            response.Close();


            return XmlString;

        }

        private void tmrWeight_Tick(object sender, EventArgs e)
        {
            lblWeightKg.Text = scale.kgWeight.ToString() + " kg";
            lblWeightLb.Text = scale.lbWeight.ToString() + " lb";
        }

        private void cmdZero_Click(object sender, EventArgs e)
        {
            scale.setZero();
        }

        private void cmdUp_Click(object sender, EventArgs e)
        {
            if (currentPage == 0)
            {
                return;
            }

            currentPage--;
            showItems();

            refreshPageInfo();
        }

        private void cmdDown_Click(object sender, EventArgs e)
        {

            if ((currentPage + 1) == maxPage)
            {
                return;
            }

            currentPage++;

            showItems();

            refreshPageInfo();

        }

        public void setCompleteButtonEnable(int isEnable)
        {
            switch (isEnable)
            {
                case 1:
                    cmdComplete.BackColor = Color.Lime;
                    cmdPrint.Enabled = true;
                    cmdComplete.Text = "完成";
                    cmdReleaseInvoice.Enabled = true;
                    break;
                case 2:
                    cmdComplete.Text = "儲存";
                    cmdPrint.Enabled = false;
                    cmdComplete.BackColor = Color.Yellow;
                    cmdReleaseInvoice.Enabled = true;
                    break;
                case 3:
                    cmdComplete.Text = "儲存";
                    cmdComplete.Enabled = false;
                    cmdPrint.Enabled = true;
                    cmdComplete.BackColor = Color.Gray;
                    cmdReleaseInvoice.Enabled = false;
                    break;
                case 4:
                    cmdReleaseInvoice.Enabled = false;
                    cmdPrint.Enabled = false;
                    cmdComplete.Enabled = true;
                    cmdComplete.Text = "儲存";
                    cmdComplete.BackColor = Color.Yellow;
                    break;
                case 5:
                    cmdReleaseInvoice.Enabled = false;
                    cmdPrint.Enabled = false;
                    cmdComplete.Enabled = true;
                    cmdComplete.Text = "完成";
                    cmdComplete.BackColor = Color.Lime;
                    break;

            }

        }

        public void checkIfCompleted()
        {
            setCompleteButtonEnable(1);

            Boolean ClosedStatusExist = false;
            Boolean OpenStatusExist = false;

            for (int i = 0; i < itemList.data.Length; i++)
            {
                if (itemList.data[i].Status.Equals("C"))
                {
                    ClosedStatusExist = true;
                }
                else
                {
                    OpenStatusExist = true;
                }


            }

            if (!ClosedStatusExist)
            {
                for (int i = 0; i < itemList.data.Length; i++)
                {
                    Boolean checkOKButton = true;

                    if (itemList.data[i].Weigh.Equals("Y"))
                    {
                        checkOKButton = false;

                        try
                        {
                            if ((postObject.Lines[i] != null) && (postObject.Lines[i].CatchWeights[0] != null) && (postObject.Lines[i].CatchWeights[0].QTY1 == 0))
                            {
                                MessageBox.Show("Iteration i: " + i.ToString());
                                MessageBox.Show(itemList.data[i].Description);

                                MessageBox.Show("DocEntry: " + postObject.DocEntry.ToString());
                                MessageBox.Show("LineNum: " + postObject.Lines[i].LineNum.ToString());

                                setCompleteButtonEnable(2);
                                return;
                            }
                        }
                        catch (Exception ex) {
                            MessageBox.Show(ex.ToString());
                        }

                        
                    }

                    if (itemList.data[i].Processing.Equals("2"))
                    {
                        checkOKButton = false;

                        try
                        {
                            if ((postObject.Lines[i] != null) && (postObject.Lines[i].CatchWeights[0] != null) && (postObject.Lines[i].CatchWeights[0].QTY2 == 0))
                            {
                                MessageBox.Show("ok2");

                                setCompleteButtonEnable(2);
                                return;
                            }
                        }
                        catch (Exception e) { }
                        
                    }

                    if (checkOKButton)
                    {
                        try
                        {
                            if (!okButtonPressed[i])
                            {
                                MessageBox.Show("ok3");

                                setCompleteButtonEnable(2);
                                return;
                            }
                        }
                        catch (Exception e) { }
                        
                    }

                }

                return;

            }

            if (!OpenStatusExist)
            {
                setCompleteButtonEnable(3);
                return;
            }

            if (ClosedStatusExist && OpenStatusExist)
            {
                setCompleteButtonEnable(5);

                for (int i = 0; i < itemList.data.Length; i++)
                {
                    Boolean checkOKButton = true;

                    if (itemList.data[i].Weigh.Equals("Y"))
                    {
                        checkOKButton = false;

                        if (postObject.Lines[i].CatchWeights[0].QTY1 == 0)
                        {
                            setCompleteButtonEnable(4);
                            return;
                        }
                    }

                    if (itemList.data[i].Processing.Equals("2"))
                    {
                        checkOKButton = false;

                        if (postObject.Lines[i].CatchWeights[0].QTY2 == 0)
                        {
                            setCompleteButtonEnable(4);
                            return;
                        }
                    }

                    if (checkOKButton)
                    {
                        if (!okButtonPressed[i])
                        {
                            setCompleteButtonEnable(4);
                            return;
                        }
                    }

                }

                return;
            }




        }

        private void cmdComplete_Click(object sender, EventArgs e)
        {
            if (cmdComplete.Text == "儲存")
            {
                tblLayout.Controls.Clear();
                lblCustomerName.Text = "";
                lblDate.Text = "";
                enableAllButtons(false);
                return;
            }

            frmIDScan form = new frmIDScan();
            form.ShowDialog();

            if (form.Response == false)
            {
                return;
            }

            postObject.CTN = Int32.Parse(form.Number);
            postObject.CreateDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            postObject.User = "";
            postObject.Device = deviceID;

            String JSonString = JsonConvert.SerializeObject(postObject);

            try
            {
                Task<String> t = Task.Run(() => postHTTP(JSonString, postAddr));

                printLabels(postObject.CTN);

                scale.pause();

                while (scale.isOpen())
                {
                    System.Threading.Thread.Sleep(100);
                }

                frmMessage waitMessage = new frmMessage("正在上傳數據...\r\n請稍等...", false);
                waitMessage.Show();


                ResponseStatus status = JsonConvert.DeserializeObject<ResponseStatus>(t.Result);

                waitMessage.Close();


                if (status.status == 204)
                {
                    tblLayout.Controls.Clear();
                    lblCustomerName.Text = "";
                    lblDate.Text = "";
                    lblInvoiceNo.Text = "";
                    enableAllButtons(false);

                    frmMessage sucessMessage = new frmMessage("上傳成功.", true, 3);
                    sucessMessage.ShowDialog();

                }
                else
                {
                    frmMessageBoxYesNo failMessage = new frmMessageBoxYesNo("上傳失敗. 重試?");
                    failMessage.ShowDialog();

                    if (failMessage.getResponse())
                    {
                        cmdComplete.PerformClick();
                    }
                    else
                    {
                        tblLayout.Controls.Clear();
                        lblCustomerName.Text = "";
                        lblDate.Text = "";
                        lblInvoiceNo.Text = "";
                        enableAllButtons(false);
                    }
                }


                scale.unpause();
            }
            catch (Exception ex)
            {
                frmMessageBoxYesNo failMessage = new frmMessageBoxYesNo("上傳失敗. 重試?");
                failMessage.ShowDialog();

                if (failMessage.getResponse())
                {
                    cmdComplete.PerformClick();
                }
                else
                {
                    tblLayout.Controls.Clear();
                    lblCustomerName.Text = "";
                    lblDate.Text = "";
                    lblInvoiceNo.Text = "";
                    enableAllButtons(false);
                }
            }


        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Print Clicked.");

            /*
            if ((postObject != null) && (postObject.CTN != 0))
            {
                printLabels(postObject.CTN);
            }
            */
            if (postObject == null)
            {
                MessageBox.Show("PostObject is null");
                return;
            }

            frmIDScan form = new frmIDScan();
            form.ShowDialog();

            if (form.Response == false)
            {
                return;
            }

            printLabels(Int32.Parse(form.Number));

        }

        private void printLabels(int copies)
        {
            MNLABEL.Label label = new MNLABEL.Label();

            //  pDocEntry = Primary Key, from Get-Web Service
            // pOrderType = "Process" value from QRCODE
            // pCount = From User Input, Number of Carton
            // pPrinter = Label Printer Name

            label.PrintLabel(DocNum, Int32.Parse(mProcess), copies, printerName);
        }

        private void cmdReleaseInvoice_Click(object sender, EventArgs e)
        {
            frmMessageBoxYesNo form = new frmMessageBoxYesNo("確定放單?");

            form.ShowDialog();

            if (!form.getResponse())
            {
                return;
            }

            DocNumJSON json = new DocNumJSON();
            json.DocNum = DocNum.ToString();
            json.Process = mProcess;

            String jsonString = JsonConvert.SerializeObject(json);

            scale.pause();

            while (scale.isOpen())
            {
                System.Threading.Thread.Sleep(100);
            }

            frmMessage waitMessage = new frmMessage("正在放單...\r\n請稍等...", false);
            waitMessage.Show();

            Task<String> t = Task.Run(() => postHTTP(jsonString, postReleaseAddr));

            waitMessage.Close();

            scale.unpause();



            Debug.WriteLine(t.Result);
            File.WriteAllText("debug.txt", t.Result);

            cmdCancel.PerformClick();

        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            tblLayout.Controls.Clear();
            lblCustomerName.Text = "";
            lblDate.Text = "";
            lblInvoiceNo.Text = "";
            enableAllButtons(false);
        }

        private void FrmMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (lblDate.Text != "")
            {
                return;
            }


            BarcodeInput += e.KeyChar.ToString();

            lblInvoiceNo.Text = BarcodeInput;

        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (lblDate.Text != "")
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }

            if (keyData == Keys.Enter)
            {
                // Enter is pressed
                processInput(BarcodeInput);
                BarcodeInput = "";

                return true; //return true if you want to suppress the key.
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void tblLayout_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row % 2 == 0)

            {

                e.Graphics.FillRectangle(new SolidBrush(SystemColors.Control), e.CellBounds);

            }

            else

            {

                e.Graphics.FillRectangle(new SolidBrush(SystemColors.ControlDark), e.CellBounds);

            }

        }
    }


    public class ItemRow
    {
        private Data data;
        public ItemLabel ItemDescription;
        public ItemLabel ItemOrderQty;
        public WeighingButton WeighButton;
        public WeighingButton ProcessButton;
        public WeighingButton confirmButton;
        public Control dummyControl;
        private frmMain form;

        public ItemRow(frmMain form, Data data)
        {
            this.data = data;

            String itemDescriptionText = data.Description;


            if ((data.ProcessingRemark != null) && (data.ProcessingRemark.Trim() != ""))
            {
                itemDescriptionText += "," + data.ProcessingRemark;
            }

            if ((data.Packing != null) && (data.Packing.Trim() != ""))
            {
                itemDescriptionText += "," + data.Packing;
            }

            ItemDescription = new ItemLabel(form, data, this, itemDescriptionText);

            String itemOrderQtyText = data.OrderQty;

            if ((data.OrderRemark != null) && (data.OrderRemark != ""))
            {
                itemOrderQtyText += "," + data.OrderRemark;
            }

            ItemOrderQty = new ItemLabel(form, data, this, itemOrderQtyText);
            WeighButton = new WeighingButton(form, data, "原料重", WeighingButton.Weight1Button);
            ProcessButton = new WeighingButton(form, data, "成品重", WeighingButton.Weight2Button);
            confirmButton = new WeighingButton(form, data, "OK", WeighingButton.okButton);
            dummyControl = new Control();

            confirmButton.Visible = false;
            confirmButton.Enabled = false;
            dummyControl.Visible = false;
            dummyControl.Enabled = false;

            if (data.Weigh.Equals("Y"))
            {
                WeighButton.Enabled = true;


            }
            else
            {
                WeighButton.Enabled = false;

            }

            if (data.Processing.Equals("2"))
            {
                ProcessButton.Enabled = true;


            }
            else
            {
                ProcessButton.Enabled = false;

            }

            if ((!data.Weigh.Equals("Y")) && (!data.Processing.Equals("2")))
            {
                confirmButton.Enabled = true;
                confirmButton.Visible = true;

                WeighButton.Visible = false;
                ProcessButton.Visible = false;

                dummyControl.Visible = true;
            }


            this.form = form;
        }
    }

    public class ItemLabel : System.Windows.Forms.Label
    {
        frmMain form;
        Data data;
        ItemRow itemRow;

        public ItemLabel(frmMain form, Data data, ItemRow itemRow, String Text)
        {
            this.Text = Text;
            Font font = new Font("Century Gothic", 20);
            this.Font = font;
            this.Dock = DockStyle.Fill;
            this.TextAlign = ContentAlignment.MiddleLeft;
            this.form = form;
            this.data = data;
            this.itemRow = itemRow;
            this.BackColor = Color.Transparent;

        }

    }

    public class WeighingButton : Button
    {
        int ButtonType;
        frmMain form;
        Data data;

        public WeighingButton(frmMain form, Data data, String Text, int ButtonType)
        {
            this.Text = Text;
            Font font = new Font("Century Gothic", 24);
            this.Font = font;
            this.Dock = DockStyle.Fill;

            this.ButtonType = ButtonType;
            this.form = form;
            this.data = data;
            this.Click += WeighingButton_Click;
        }

        private void WeighingButton_Click(object sender, EventArgs e)
        {
            int index = Array.IndexOf(form.itemList.data, data);

            Decimal kgWeight = form.scale.kgWeight;
            Decimal lbWeight = form.scale.lbWeight;

            // get the current line
            Line currentLine = null;
            foreach (Line line in form.postObject.Lines)
            {
                if (line.LineNum == form.itemList.data[index].LineNum)
                {
                    currentLine = line;
                    break;

                }
            }

            if (currentLine == null)
            {
                return;
            }

            switch (ButtonType)
            {
                case Weight1Button:

                    if (kgWeight <= 0.0m)
                    {
                        // used for testing
                        // Random rnd = new Random();
                        // kgWeight = rnd.Next(1, 100);
                        return;
                    }

                    // if button 2 has value, do nothing
                    if (currentLine.CatchWeights[0].QTY2 != 0.0)
                    {
                        return;
                    }


                    if (this.BackColor != Color.Lime)
                    {

                        currentLine.CatchWeights[0].QTY1 = float.Parse(kgWeight.ToString());
                        currentLine.CatchWeights[0].UOM1 = "kg";
                        this.Text = kgWeight + " kg\r\n" + lbWeight + " lb";
                        MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);
                        this.BackColor = Color.Lime;


                    }
                    else
                    {


                        frmOverwriteWeight formOverwrite = new frmOverwriteWeight();
                        formOverwrite.ShowDialog();

                        switch (formOverwrite.getResponse())
                        {
                            case frmOverwriteWeight.RESPONSE_CLEAR:

                                currentLine.CatchWeights[0].QTY1 = float.Parse("0");
                                currentLine.CatchWeights[0].UOM1 = "kg";
                                this.BackColor = SystemColors.Control;
                                this.Text = "原料重";
                                MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);


                                break;


                            case frmOverwriteWeight.RESPONSE_ADD:

                                currentLine.CatchWeights[0].QTY1 = float.Parse(kgWeight.ToString()) + currentLine.CatchWeights[0].QTY1;
                                currentLine.CatchWeights[0].UOM1 = "kg";
                                this.Text = currentLine.CatchWeights[0].QTY1.ToString("F2") + " kg\r\n" + Math.Round(currentLine.CatchWeights[0].QTY1 * 2.205, 2) + " lb";
                                MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);
                                this.BackColor = Color.Lime;

                                break;

                        }
                        break;

                    }
                    break;

                case Weight2Button:
                    if (kgWeight <= 0.0m)
                    {
                        // used for testing
                        // Random rnd = new Random();
                        // kgWeight = rnd.Next(1, 100);

                        return;
                    }

                    // if button 2 has value, do nothing
                    if (currentLine.CatchWeights[0].QTY1 == 0.0)
                    {
                        return;
                    }

                    if (this.BackColor != Color.Lime)
                    {


                        // if button 1 has no value, do nothing
                        if (currentLine.CatchWeights[0].QTY1 == 0.0)
                        {
                            return;
                        }


                        currentLine.CatchWeights[0].QTY2 = float.Parse(kgWeight.ToString());
                        currentLine.CatchWeights[0].UOM2 = "kg";
                        this.Text = kgWeight + " kg\r\n" + lbWeight + " lb";
                        MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);
                        this.BackColor = Color.Lime;



                    }
                    else
                    {
                        // this is for if the button had already been pressed.

                        frmOverwriteWeight formOverwrite2 = new frmOverwriteWeight();
                        formOverwrite2.ShowDialog();

                        switch (formOverwrite2.getResponse())
                        {
                            case frmOverwriteWeight.RESPONSE_CLEAR:

                                currentLine.CatchWeights[0].QTY2 = float.Parse("0");
                                currentLine.CatchWeights[0].UOM2 = "kg";
                                this.BackColor = SystemColors.Control;
                                this.Text = "成品重";
                                MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);

                                break;


                            case frmOverwriteWeight.RESPONSE_ADD:

                                currentLine.CatchWeights[0].QTY2 = float.Parse(kgWeight.ToString()) + currentLine.CatchWeights[0].QTY2;
                                currentLine.CatchWeights[0].UOM2 = "kg";
                                this.Text = currentLine.CatchWeights[0].QTY2.ToString("F2") + " kg\r\n" + Math.Round(currentLine.CatchWeights[0].QTY2 * 2.205, 2) + " lb";
                                MAXMSQL.updateCatchWeight(form.connectionString, currentLine, currentLine.CatchWeights[0], form.DocEntry);
                                this.BackColor = Color.Lime;

                                break;

                        }
                    }

                    break;

                case okButton:


                    form.okButtonPressed[index] = !form.okButtonPressed[index];
                    if (form.okButtonPressed[index])
                    {
                        MAXMSQL.SetOKPressed(form.connectionString, form.postObject.Lines[index], form.DocEntry, 1);
                        this.BackColor = Color.Lime;
                    }
                    else
                    {
                        MAXMSQL.SetOKPressed(form.connectionString, form.postObject.Lines[index], form.DocEntry, 0);
                        this.BackColor = SystemColors.Control;
                    }


                    break;
            }

            form.checkIfCompleted();


        }

        public void setWeighingButtonText(String text)
        {
            this.Text = text;
            this.BackColor = Color.Lime;
        }

        public const int Weight1Button = 0;
        public const int Weight2Button = 1;
        public const int okButton = 2;
    }

}

#region HTTPGet classes
public struct Order
{
    public String DocNum;
    public String Process;
}

public class ItemList
{
    public Data[] data { get; set; }
    public int total { get; set; }
}

public class Data
{
    public string Description { get; set; }
    public string DocDate { get; set; }
    public string CardName { get; set; }
    public int DocNum { get; set; }
    public int DocEntry { get; set; }
    public int LineNum { get; set; }
    public string OrderQty { get; set; }
    public string Weigh { get; set; }
    public string Processing { get; set; }
    public string Packing { get; set; }
    public string ProcessingRemark { get; set; }
    public string OrderRemark { get; set; }
    public string Status { get; set; }
    public string Department { get; set; }
    public string DeliveryAdress { get; set; }
    public string ShipMethod { get; set; }
    public string ShipGroup { get; set; }
    public string PackType { get; set; }
    public string BPDistrict { get; set; }
    public string ActualCatch { get; set; }
    public string U_ImportQty { get; set; }
    public string SuggestPick { get; set; }
}
#endregion


#region httpPost classes
public class PostObject
{
    public int DocEntry { get; set; }
    public int PickType { get; set; }
    public int CTN { get; set; }
    public string CreateDateTime { get; set; }
    public string User { get; set; }
    public string Device { get; set; }
    public Line[] Lines { get; set; }
}

public class Line
{
    public int LineNum { get; set; }
    public string Weigh { get; set; }
    public Catchweight[] CatchWeights { get; set; }

}

public class Catchweight
{
    public float QTY1 { get; set; }
    public float QTY2 { get; set; }
    public string UOM1 { get; set; }
    public string UOM2 { get; set; }
}
#endregion


#region response classes
public class ResponseStatus
{
    public ResponseData[] data { get; set; }
    public int status { get; set; }
}

public class ResponseData
{
    public int DocEntry { get; set; }
}
#endregion

[Serializable]
public struct InitialisationXML
{
    public string postHTTP;
    public string postReleaseHTTP;
    public string getHTTP;
    public string printerName;
    public string deviceID;
    public string DB_IP;
    public int DB_port;
    public string DB_login;
    public string DB_password;
}


public class DocNumJSON
{
    public string DocNum { get; set; }
    public string Process { get; set; }
}



