﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAXM401_20191029_TouchScreenScale
{
    public partial class frmIDScan : Form
    {
        //public string ID { get; set; } = "";
        public string Number { get; set; } = "";
        public Boolean Response { get; set; } = true;

        public frmIDScan()
        {
            InitializeComponent();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Response = false;
            Number = "";

            this.Close();
        }

        private void cmdClear_Click(object sender, EventArgs e)
        {
            lblNumber.Text = "0";
        }

        private void cmdKeyPress_Click(Object sender, EventArgs e)
        {
            Button button = (Button)sender;
            keyPress(button.Text);
        }

        private void keyPress(string text)
        {
            switch (text)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    if (lblNumber.Text == "0")
                    {
                        lblNumber.Text = text;
                    } else
                    {
                        if (lblNumber.Text.Length < 3)
                        {
                            lblNumber.Text += text;
                        }
                    }
                    break;
                case "0":
                    if (lblNumber.Text != "0")
                    {
                        if (lblNumber.Text.Length < 3)
                        {
                            lblNumber.Text += text;
                        }
                    }
                    break;
             }
            
        }

        //private void frmIDScan_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    ID += e.KeyChar.ToString();

        //    if (ID.Length == 10)
        //    {
        //        processID();
        //    }

        //}

        //private void processID()
        //{

        //}

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (lblNumber.Text == "0")
            {
                frmMessage form = new frmMessage("件數不能設為零");
                form.ShowDialog();
                return;
            }

            Number = lblNumber.Text;

            this.Close();
        }
    }
}