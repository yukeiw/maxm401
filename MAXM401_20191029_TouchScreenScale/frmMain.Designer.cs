﻿namespace MAXM401_20191029_TouchScreenScale
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlWeight = new System.Windows.Forms.Panel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblWeightLb = new System.Windows.Forms.Label();
            this.lblWeightKg = new System.Windows.Forms.Label();
            this.lblDateLabel = new System.Windows.Forms.Label();
            this.lblInvoiceNoLabel = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.tmrWeight = new System.Windows.Forms.Timer(this.components);
            this.tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this.cmdComplete = new System.Windows.Forms.Button();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.cmdReleaseInvoice = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdZero = new System.Windows.Forms.Button();
            this.cmdPrint = new System.Windows.Forms.Button();
            this.cmdDown = new System.Windows.Forms.Button();
            this.cmdUp = new System.Windows.Forms.Button();
            this.pnlWeight.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlWeight
            // 
            this.pnlWeight.Controls.Add(this.lblVersion);
            this.pnlWeight.Controls.Add(this.lblWeightLb);
            this.pnlWeight.Controls.Add(this.lblWeightKg);
            this.pnlWeight.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWeight.Location = new System.Drawing.Point(0, 0);
            this.pnlWeight.Name = "pnlWeight";
            this.pnlWeight.Size = new System.Drawing.Size(1024, 104);
            this.pnlWeight.TabIndex = 1;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.MidnightBlue;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(884, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(140, 23);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "label1";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWeightLb
            // 
            this.lblWeightLb.BackColor = System.Drawing.Color.MidnightBlue;
            this.lblWeightLb.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWeightLb.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightLb.ForeColor = System.Drawing.Color.Red;
            this.lblWeightLb.Location = new System.Drawing.Point(512, 0);
            this.lblWeightLb.Name = "lblWeightLb";
            this.lblWeightLb.Size = new System.Drawing.Size(512, 104);
            this.lblWeightLb.TabIndex = 2;
            this.lblWeightLb.Text = "0 lb";
            this.lblWeightLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWeightKg
            // 
            this.lblWeightKg.BackColor = System.Drawing.Color.MidnightBlue;
            this.lblWeightKg.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWeightKg.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightKg.ForeColor = System.Drawing.Color.Green;
            this.lblWeightKg.Location = new System.Drawing.Point(0, 0);
            this.lblWeightKg.Name = "lblWeightKg";
            this.lblWeightKg.Size = new System.Drawing.Size(512, 104);
            this.lblWeightKg.TabIndex = 1;
            this.lblWeightKg.Text = "0 kg";
            this.lblWeightKg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateLabel
            // 
            this.lblDateLabel.AutoSize = true;
            this.lblDateLabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateLabel.Location = new System.Drawing.Point(576, 117);
            this.lblDateLabel.Name = "lblDateLabel";
            this.lblDateLabel.Size = new System.Drawing.Size(130, 33);
            this.lblDateLabel.TabIndex = 3;
            this.lblDateLabel.Text = "出貨日期:";
            // 
            // lblInvoiceNoLabel
            // 
            this.lblInvoiceNoLabel.AutoSize = true;
            this.lblInvoiceNoLabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvoiceNoLabel.Location = new System.Drawing.Point(630, 161);
            this.lblInvoiceNoLabel.Name = "lblInvoiceNoLabel";
            this.lblInvoiceNoLabel.Size = new System.Drawing.Size(76, 33);
            this.lblInvoiceNoLabel.TabIndex = 10;
            this.lblInvoiceNoLabel.Text = "單號:";
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(712, 117);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(300, 33);
            this.lblDate.TabIndex = 11;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerName.Location = new System.Drawing.Point(12, 117);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(558, 73);
            this.lblCustomerName.TabIndex = 12;
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tmrWeight
            // 
            this.tmrWeight.Tick += new System.EventHandler(this.tmrWeight_Tick);
            // 
            // tblLayout
            // 
            this.tblLayout.ColumnCount = 4;
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.93103F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.68966F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.68966F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.68966F));
            this.tblLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblLayout.Location = new System.Drawing.Point(12, 204);
            this.tblLayout.Name = "tblLayout";
            this.tblLayout.RowCount = 4;
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tblLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblLayout.Size = new System.Drawing.Size(1000, 507);
            this.tblLayout.TabIndex = 15;
            this.tblLayout.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tblLayout_CellPaint);
            // 
            // cmdComplete
            // 
            this.cmdComplete.Enabled = false;
            this.cmdComplete.FlatAppearance.BorderSize = 0;
            this.cmdComplete.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdComplete.Location = new System.Drawing.Point(881, 717);
            this.cmdComplete.Name = "cmdComplete";
            this.cmdComplete.Size = new System.Drawing.Size(131, 42);
            this.cmdComplete.TabIndex = 8;
            this.cmdComplete.Text = "完成";
            this.cmdComplete.UseVisualStyleBackColor = true;
            this.cmdComplete.Click += new System.EventHandler(this.cmdComplete_Click);
            // 
            // lblPageNo
            // 
            this.lblPageNo.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(439, 716);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(147, 51);
            this.lblPageNo.TabIndex = 4;
            this.lblPageNo.Text = "第 1 / 1 頁";
            this.lblPageNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvoiceNo.Location = new System.Drawing.Point(712, 161);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(300, 33);
            this.lblInvoiceNo.TabIndex = 18;
            this.lblInvoiceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdReleaseInvoice
            // 
            this.cmdReleaseInvoice.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdReleaseInvoice.Enabled = false;
            this.cmdReleaseInvoice.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdReleaseInvoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdReleaseInvoice.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdReleaseInvoice.ForeColor = System.Drawing.Color.White;
            this.cmdReleaseInvoice.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.放單2;
            this.cmdReleaseInvoice.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdReleaseInvoice.Location = new System.Drawing.Point(153, 717);
            this.cmdReleaseInvoice.Name = "cmdReleaseInvoice";
            this.cmdReleaseInvoice.Size = new System.Drawing.Size(120, 42);
            this.cmdReleaseInvoice.TabIndex = 16;
            this.cmdReleaseInvoice.UseVisualStyleBackColor = false;
            this.cmdReleaseInvoice.Click += new System.EventHandler(this.cmdReleaseInvoice_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.BackColor = System.Drawing.Color.IndianRed;
            this.cmdCancel.Enabled = false;
            this.cmdCancel.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.ForeColor = System.Drawing.Color.White;
            this.cmdCancel.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.退出;
            this.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdCancel.Location = new System.Drawing.Point(12, 717);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(121, 42);
            this.cmdCancel.TabIndex = 17;
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdZero
            // 
            this.cmdZero.BackColor = System.Drawing.Color.LightGreen;
            this.cmdZero.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.cmdZero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdZero.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdZero.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdZero.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.return0White;
            this.cmdZero.Location = new System.Drawing.Point(691, 717);
            this.cmdZero.Name = "cmdZero";
            this.cmdZero.Size = new System.Drawing.Size(78, 42);
            this.cmdZero.TabIndex = 14;
            this.cmdZero.UseVisualStyleBackColor = false;
            this.cmdZero.Click += new System.EventHandler(this.cmdZero_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.BackColor = System.Drawing.Color.SandyBrown;
            this.cmdPrint.Enabled = false;
            this.cmdPrint.FlatAppearance.BorderColor = System.Drawing.Color.SandyBrown;
            this.cmdPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrint.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrint.ForeColor = System.Drawing.Color.White;
            this.cmdPrint.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.PrintWhite;
            this.cmdPrint.Location = new System.Drawing.Point(784, 717);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(82, 42);
            this.cmdPrint.TabIndex = 7;
            this.cmdPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdPrint.UseVisualStyleBackColor = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdDown
            // 
            this.cmdDown.Enabled = false;
            this.cmdDown.FlatAppearance.BorderSize = 0;
            this.cmdDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDown.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDown.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.Right;
            this.cmdDown.Location = new System.Drawing.Point(580, 718);
            this.cmdDown.Name = "cmdDown";
            this.cmdDown.Size = new System.Drawing.Size(74, 42);
            this.cmdDown.TabIndex = 6;
            this.cmdDown.UseVisualStyleBackColor = true;
            this.cmdDown.Click += new System.EventHandler(this.cmdDown_Click);
            // 
            // cmdUp
            // 
            this.cmdUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.cmdUp.Enabled = false;
            this.cmdUp.FlatAppearance.BorderSize = 0;
            this.cmdUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdUp.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdUp.Image = global::MAXM401_20191029_TouchScreenScale.Properties.Resources.Left;
            this.cmdUp.Location = new System.Drawing.Point(370, 718);
            this.cmdUp.Name = "cmdUp";
            this.cmdUp.Size = new System.Drawing.Size(74, 42);
            this.cmdUp.TabIndex = 6;
            this.cmdUp.UseVisualStyleBackColor = true;
            this.cmdUp.Click += new System.EventHandler(this.cmdUp_Click);
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblInvoiceNo);
            this.Controls.Add(this.cmdReleaseInvoice);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdComplete);
            this.Controls.Add(this.cmdZero);
            this.Controls.Add(this.cmdPrint);
            this.Controls.Add(this.tblLayout);
            this.Controls.Add(this.cmdDown);
            this.Controls.Add(this.lblPageNo);
            this.Controls.Add(this.cmdUp);
            this.Controls.Add(this.lblCustomerName);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblInvoiceNoLabel);
            this.Controls.Add(this.lblDateLabel);
            this.Controls.Add(this.pnlWeight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmMain_KeyPress);
            this.pnlWeight.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlWeight;
        private System.Windows.Forms.Label lblWeightLb;
        private System.Windows.Forms.Label lblWeightKg;
        private System.Windows.Forms.Label lblDateLabel;
        private System.Windows.Forms.Label lblInvoiceNoLabel;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Timer tmrWeight;
        private System.Windows.Forms.TableLayoutPanel tblLayout;
        private System.Windows.Forms.Button cmdReleaseInvoice;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdDown;
        private System.Windows.Forms.Button cmdUp;
        private System.Windows.Forms.Button cmdPrint;
        private System.Windows.Forms.Button cmdZero;
        private System.Windows.Forms.Button cmdComplete;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.Label lblInvoiceNo;
        private System.Windows.Forms.Label lblVersion;
    }
}

