﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAXM401_20191029_TouchScreenScale
{
    public partial class frmMessageBoxYesNo : Form
    {
        private bool response;

        public bool getResponse() { return response; }

        public frmMessageBoxYesNo(String Message)
        {
            InitializeComponent();
            lblMessage.Text = Message;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            response = true;
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            response = false;
            this.Close();
        }
    }
}
