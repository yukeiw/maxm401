﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;

namespace MAXM401_20191126_MSSQLTest
{
    public class MAXMSQL
    {
        public static PostObject getPostObject()
        {
            return new PostObject();
        }

        public static void savePostObject (String ConnectionString, PostObject postObject)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);


            /*
            String commandStr = String.Format(@"
                INSERT INTO postobject (DocEntry, PickType, CTN) 
                    VALUES ({0},{1},{2})
                ON DUPLICATE KEY UPDATE
                    Picktype={1},
                    CTN={2}"
                    ,postObject.DocEntry, postObject.PickType, postObject.CTN);
                    */

            String commandStr = String.Format(@"
                IF EXISTS (SELECT 1 FROM postobject where DocEntry = {0})

                UPDATE postobject set CTN={1}, picktype={2}, [user]='{3}' WHERE docEntry = {0}
                
                ELSE

                INSERT INTO postobject (DocEntry, CTN, picktype, [user]) VALUES ({0}, {1}, {2}, '{3}');

            ", postObject.DocEntry, postObject.CTN, postObject.PickType, postObject.User);

            Debug.WriteLine(commandStr);
            

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
            
        }


        public static void deleteALLPostObject(String ConnectionString)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);


            /*
            String commandStr = String.Format(@"
                INSERT INTO postobject (DocEntry, PickType, CTN) 
                    VALUES ({0},{1},{2})
                ON DUPLICATE KEY UPDATE
                    Picktype={1},
                    CTN={2}"
                    ,postObject.DocEntry, postObject.PickType, postObject.CTN);
                    */

            String commandStr = String.Format(@"
                DELETE FROM PostObject;
                DELETE FROM line;
                DELETE FROM catchweight
;
            "
);

            Debug.WriteLine(commandStr);


            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();

        }

        public static void SaveLine (String ConnectionString, Line line, int DocNum)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                INSERT INTO line (LineNum, Weigh, PostObjectDocEntry) 
                    VALUES ({0},'{1}',{2})
                ON DUPLICATE KEY UPDATE
                    Weigh='{1}'"
                    , line.LineNum, line.Weigh, DocNum);


            Debug.WriteLine(commandStr);


            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();

        }

        public static void insertIgnoreCatchWeight(String ConnectionString, Line line, Catchweight catchweight, int DocNum)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                INSERT IGNORE INTO catchweight (Line, DocEntry) 
                    VALUES ({0},{1})"
                
                    , line.LineNum, DocNum);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void updateCatchWeight(String ConnectionString, Line line, Catchweight catchweight, int DocNum)
        { 
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                UPDATE catchweight SET 
                    QTY1={2},
                    QTY2={3},
                    UOM1='{4}',
                    UOM2='{5}'
                WHERE Line = {0} AND DocEntry = {1};",
                line.LineNum, DocNum, catchweight.QTY1, catchweight.QTY2, catchweight.UOM1, catchweight.UOM2);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static void SetOKPressed(String ConnectionString, Line line, int DocNum)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                UPDATE catchweight SET 
                    okPressed = 1
                WHERE Line = {0} AND DocEntry = {1};",
                line.LineNum, DocNum);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            command.ExecuteNonQuery();

            conn.Close();
        }
    
        public static string getConnectionString(String IP, int port, String Database, String ID, String Password)
        {
            return String.Format("Server = {0},{1}; Database = {2}; Uid = {3}; Pwd = {4};", IP, port, Database, ID, Password);
        }

        public static PostObject getPostObject(String ConnectionString, int DocEntry, ref Boolean[] okButtonPressed)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                SELECT * FROM postObject WHERE DocEntry = {0}",
                DocEntry);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                return new PostObject();
            }

            reader.Read();

            PostObject returnPostObject = new PostObject();
            returnPostObject.CreateDateTime = reader["CreateDateTime"].ToString();
            returnPostObject.DocEntry = (Int32)reader["DocEntry"];
            returnPostObject.PickType = (Int32)reader["PickType"];
            returnPostObject.CTN = (Int32)reader["CTN"];
            returnPostObject.User = reader["User"].ToString();


            returnPostObject.Lines = getLines(ConnectionString, DocEntry, ref okButtonPressed);

            conn.Close();

            return returnPostObject;
        }

        public static PostObject getPostObject(String ConnectionString, int DocEntry)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                SELECT * FROM postObject WHERE DocEntry = {0}",
                DocEntry);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            if (!reader.HasRows)
            {
                return new PostObject();
            }

            reader.Read();

            PostObject returnPostObject = new PostObject();
            returnPostObject.CreateDateTime = reader["CreateDateTime"].ToString();
            returnPostObject.DocEntry = (Int32)reader["DocEntry"];
            returnPostObject.PickType = (Int32)reader["PickType"];
            returnPostObject.CTN = (Int32)reader["CTN"];
            returnPostObject.User = reader["User"].ToString();

            conn.Close();

            return returnPostObject;
        }

        public static Line[] getLines(String ConnectionString, int DocEntry, ref Boolean[] okButtonPressed)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            String commandStr = String.Format(@"
                SELECT * FROM line AS a
                LEFT JOIN
                catchweight AS b
                ON
                b.DocEntry = a.PostObjectDocEntry AND b.Line = a.LineNum
                WHERE PostObjectDocEntry = {0};",
                DocEntry);

            Debug.WriteLine(commandStr);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = commandStr;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader();

            List<Line> lineList = new List<Line>();
            List<Boolean> okButtonPressedList = new List<Boolean>();

            while (reader.Read())
            {
                Line line = new Line();
                line.LineNum = (Int32)reader["LineNum"];
                line.Weigh = reader["Weigh"].ToString();
                line.CatchWeights = new Catchweight[1];
                line.CatchWeights[0] = new Catchweight();
                line.CatchWeights[0].QTY1 = (float)reader["QTY1"];
                line.CatchWeights[0].QTY2 = (float)reader["QTY2"];
                line.CatchWeights[0].UOM1 = reader["UOM1"].ToString();
                line.CatchWeights[0].UOM2 = reader["UOM2"].ToString();

                lineList.Add(line);
                okButtonPressedList.Add(ConvertIntToBool((Int32)reader["okPressed"]));

            }

            okButtonPressed = okButtonPressedList.ToArray();

            conn.Close();

            return lineList.ToArray();

        }

        private static bool ConvertIntToBool(int v)
        {
            return (v==1);
        }
    }
}

#region httpPost classes
public class PostObject
{
    public int DocEntry { get; set; }
    public int PickType { get; set; }
    public int CTN { get; set; }
    public string CreateDateTime { get; set; }
    public string User { get; set; }
    public string Device { get; set; }
    public Line[] Lines { get; set; }
}

public class Line
{
    public int LineNum { get; set; }
    public string Weigh { get; set; }
    public Catchweight[] CatchWeights { get; set; }
}

public class Catchweight
{
    public float QTY1 { get; set; }
    public float QTY2 { get; set; }
    public string UOM1 { get; set; }
    public string UOM2 { get; set; }
}
#endregion
