﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace MAXM401_20191128_WebServiceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            String getAddr = "https://go.millionhk.com:3000/scale/getSOMAInfo";

            Order order = new Order();
            Console.Write("DocNum:");
            order.DocNum = Console.ReadLine();
            Console.Write("Process:");
            order.Process = Console.ReadLine(); ;

            // Below code is for production
            Task<String> t = Task.Run(() => getHTTP(order, getAddr));

            Console.WriteLine(t.Result);

            Console.ReadLine();

        }

        private static async Task<String> getHTTP(Order order, string addr)
        {
            String Options = String.Format("?DocNum={0}&Process={1}", order.DocNum, order.Process);
            //String Options = String.Format("?DocNum={0}", order.DocNum, order.Process);
            String url = addr + Options;


            WebRequest request = WebRequest.Create(url);

            WebResponse response = request.GetResponse();

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            String XmlString = reader.ReadToEnd();

            if (XmlString.Equals(""))
            {
                return "";
            }

            reader.Close();
            response.Close();

            return XmlString;

        }
    }

    #region HTTPGet classes
    public struct Order
    {
        public String DocNum;
        public String Process;
    }

    public class ItemList
    {
        public Data[] data { get; set; }
        public int total { get; set; }
    }

    public class Data
    {
        public string Description { get; set; }
        public string DocDate { get; set; }
        public string CardName { get; set; }
        public int DocNum { get; set; }
        public int DocEntry { get; set; }
        public int LineNum { get; set; }
        public string OrderQty { get; set; }
        public string Weigh { get; set; }
        public string Processing { get; set; }
        public string ProcessingRemark { get; set; }
        public string OrderRemark { get; set; }
        public string Status { get; set; }
        public string Department { get; set; }
        public string DeliveryAdress { get; set; }
        public string ShipMethod { get; set; }
        public string BPDistrict { get; set; }
    }
    #endregion
}
